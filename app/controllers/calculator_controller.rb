class CalculatorController < ApplicationController

    def home#menu da calculadora
        @num1 = params[:num1]
        @num2 = params[:num2]
        @result = @num1+@num2
    end
    
    def sum#funcao de somar
        @num1 = params[:num1].to_f
        @num2 = params[:num2].to_f
        @result = @num1+@num2
    end

    def minus#funcao de subtrair
        @num1 = params[:num1].to_f
        @num2 = params[:num2].to_f
        @result = @num1-@num2
    end

    def multiply#funcao de multiplicar
        @num1 = params[:num1].to_f
        @num2 = params[:num2].to_f
        @result = @num1*@num2
    end

    def division#funcao de divisao
        @num1 = params[:num1].to_f
        @num2 = params[:num2].to_f
        @result = @num1/@num2
    end
end