Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get '/apresentacao/:name/:number', to: 'apresentation#home', as:'home'
  get '/comida', to: 'food#now', as: 'food_now'
  get 'calculadora/:num1/:num2', to: 'calculator#home', as:'calculator_home'
  get 'calculadora/soma/:num1/:num2', to:'calculator#sum', as: 'calculator_sum'
  get 'calculadora/subtracao/:num1/:num2', to:'calculator#minus', as: 'calculator_minus'
  get 'calculadora/multiplicacao/:num1/:num2', to:'calculator#multiply', as: 'calculator_multiply'
  get 'calculadora/divisao/:num1/:num2', to:'calculator#division', as: 'calculator_division'
end
